/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package correctorpalabras;

/**
 *
 * @author Ray
 */
public class palabraDiccionario {

    /**
     * @return the palabra
     */
    public String getPalabra() {
        return palabra;
    }

    /**
     * @param palabra the palabra to set
     */
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    /**
     * @return the longitud
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the frecuencia
     */
    public int getFrecuencia() {
        return frecuencia;
    }

    /**
     * @param frecuencia the frecuencia to set
     */
    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }

    /**
     * @return the coincidencias
     */
    public int getCoincidencias() {
        return coincidencias;
    }

    /**
     * @param coincidencias the coincidencias to set
     */
    public void setCoincidencias(int coincidencias) {
        this.coincidencias = coincidencias;
    }
    private String palabra;
    private int longitud;
    private int frecuencia;
    private int coincidencias;
}
