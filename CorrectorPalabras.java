/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package correctorpalabras;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Ray
 */
public class CorrectorPalabras {
    
    public ArrayList<palabraDiccionario> lecturaDiccionario(String ubicacion)
    {
        ArrayList<palabraDiccionario> listaDiccionario = new ArrayList<palabraDiccionario>();
        palabraDiccionario palabra = null;
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String linea;
        String[] separado ;
        try {
            archivo = new File (ubicacion);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            linea = "";
            while((linea=br.readLine())!=null)
            {
                palabra = new palabraDiccionario();
                separado =  linea.split(",");
                //System.out.println( separado[0] + " " + separado[1] );
                palabra.setPalabra(separado[0]);
                palabra.setLongitud(separado[0].length());
                palabra.setFrecuencia(Integer.parseInt(separado[1]));
                listaDiccionario.add(palabra);
            }
                
            fr.close();
      }
      catch(Exception e){
         e.printStackTrace();
      }
        return listaDiccionario;
    }
    
    public void ordenarArrayList(ArrayList<palabraDiccionario> listaDiccionario)
    {
        int i;
        int j;
        int temp;
        palabraDiccionario palabra = null;
        palabraDiccionario palabraAux = null;
        ArrayList<palabraDiccionario> listaAux = (ArrayList) listaDiccionario.clone();
        for(i = 0; i < listaDiccionario.size(); i++)
        {
            for(j = 0; j< listaDiccionario.size()-1; j++)
            {
                
                if(listaDiccionario.get(j).getLongitud() > listaDiccionario.get(j+1).getLongitud())
                {
                    palabra = listaDiccionario.get(j);
                    palabraAux = listaDiccionario.get(j+1);
                    listaAux.remove(j);
                    listaAux.add(j ,palabraAux);
                    listaAux.remove(j+1);
                    listaAux.add(j+1 ,palabra);
                }   
                System.out.println("i " + i + " j " + j);
            }
            listaDiccionario = listaAux;
            //System.out.println("i " + i);
        }
        System.out.println("Tama�o : " + listaDiccionario.size());
        //return listaDiccionario;
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CorrectorPalabras cp = new CorrectorPalabras();
        ArrayList<palabraDiccionario> listaDiccionario = cp.lecturaDiccionario("C:\\Users\\Ray\\OneDrive\\Documents\\NetBeansProjects\\correctorPalabras\\src\\correctorpalabras\\diccionario.txt");
        cp.ordenarArrayList(listaDiccionario);
    }
    
}
